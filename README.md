# Tetris
This is a recreation of classic Tetris writen in Pascal. It runs in terminal, so graphics is a bit limited. This version also allows to create custom blocks and change width of the space.

## How to install it
Download `tetris.pas` from last commit. Compile it in terminal using FreePascal by running `fpc tetris.pas`. And finally run it `./tetris`.

## Settings
The game speed can vary. Because of that, before playing it is recommended to set speed constant to compensate these speed differences. The constant can be set in **Speed up/slow down** in **Settings**. Note that **Speed** in **Settings** only changes initial falling speed of blocks.