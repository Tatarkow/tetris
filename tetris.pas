program tetris;

{Tetris}
{William Tatarko}
{letni semestr 2017}
{Programovani 2 (NMIN102)}

uses crt; {for drawing}

const
	{Screens}
	scr_quit = 0;
	scr_menu = 1;
	scr_game = 2;
	scr_settings = 3;
	scr_settings_blocks = 4;
	scr_block_editor = 5;
	scr_loss = 6;
	scr_settings_correlation = 7;

	{Colors}
	bg_color = black;
	fg_color = lightgray;

type
	matrix_4x4 = array[1..4, 1..4] of boolean; {for block shapes}
	list = array[1..60] of record {for all blocks}
		shape: matrix_4x4;
		active: boolean;
		chance: byte;
		color: byte;
		end;

var
	scr: byte; {current scr}
	edited_block: byte; {for the block editor}
	speed, width, nu_blocks: byte; {settings}
	score, best: longint; {score}
	blocks: list; {array of all available blocks}
	speed_correlation: integer; {For differently fast computers}
{$R+}
{Helper functions/procedures}

function min(a, b: byte): byte;
	{Returns minimum from a and b}
	begin
		if a >= b then min := b
		else min := a;
	end;

function byte_to_str(x: byte): string;
	{Returns byte as a string}
	var
		s: string;

	begin
		if x = 0 then s := '0'
		else s := '';

		while x > 0 do
			begin
				s := chr(x mod 10 + ord('0')) + s;
				x := x div 10;
			end;

		byte_to_str := s;
	end;

function int_to_str(x: integer): string;
	{Returns integer as a string}
	var
		s: string;

	begin
		if x = 0 then s := '0'
		else s := '';

		while x > 0 do
			begin
				s := chr(x mod 10 + ord('0')) + s;
				x := x div 10;
			end;

		int_to_str := s;
	end;

function char_to_byte(ch: char): byte;
	{Returns charecter as a byte}
	begin
		char_to_byte := ord(ch) - ord('0');
	end;

function str_to_byte(s: string): byte;
	{Returns string as a byte}
	var
		x: byte; {temporary output}
		i: byte; {index}

	begin
		x := char_to_byte(s[1]);
		for i := 2 to length(s) do x := 10*x + char_to_byte(s[i]);
		str_to_byte := x;
	end;

function exists(file_name: string): boolean;
	{Checks if file exists}
	var
		f: text; {file}

	begin
		assign(f, file_name);
		{$I-} {turns off error-checking}
		reset(f);
		close(f);
		{$I+} {turns back on error-checking}

		{Check if an error has occured or not}
		if ioresult = 0 then exists := true
		else exists := false; 

	end;

procedure read_settings(var speed_correlation: integer;
						var speed, width, nu_blocks: byte;
						var blocks: list);
	{Reads settings and saves it to global variables}
	var
		f: text; {file}
		i: byte; {index}
		x, y: byte; {coordinates for block shape}
		temp: string; {temporary string used for reading different settings}

	begin
		assign(f, 'settings.txt');

		{Create default "settings" file, if it does not exist already}
		if not exists('settings.txt') then
			begin
				rewrite(f);

				writeln(f, '20'); {default speed correlation constant}
				writeln(f, '4'); {default speed}
				writeln(f, '10'); {default width}

				{Default blocks}
				writeln(f, '0000011000100010152');
				writeln(f, '0000011000110000155');
				writeln(f, '0100010001000100151');
				writeln(f, '0000011001100000154');
				writeln(f, '0000010001100100153');
				writeln(f, '0000010001100010156');
				writeln(f, '0000001000100110154');

				close(f);
			end;

		reset(f);

		readln(f, speed_correlation); {read speed correlation constant}
		readln(f, speed); {read speed}

		{Read width}
		readln(f, temp);
		width := str_to_byte(temp);

		{Read blocks}
		nu_blocks := 0; {total number of blocks}
		while not eof(f) do
			begin
				inc(nu_blocks);
				readln(f, temp);
				for i := 1 to 16 do {save every pixel of the current block}
					begin
						y := (i-1) div 4 + 1;
						x := (i-1) mod 4 + 1;
						blocks[nu_blocks].shape[y][x] := temp[i] = '1';
					end;
				blocks[nu_blocks].active := temp[17] = '1';
				blocks[nu_blocks].chance := char_to_byte(temp[18]);
				blocks[nu_blocks].color := char_to_byte(temp[19]);
			end;

		close(f);
	end;

procedure save_settings(index: byte; new: string);
	{Saves settings, "index" indicates which line of settings is going to be}
	{modified and "new" is the new value of the line which is going to be}
	{changed}
	var
		f: text; {file}
		i, j: byte; {indices}
		data: array[1..63] of string; {each for every line (3 + max 60 blocks)}

	begin
		assign(f, 'settings.txt');
		
		{Read settings in order to rewrite only one line}
		reset(f);
		i := 0;
		while not eof(f) do begin inc(i); readln(f, data[i]); end;
		close(f);

		{Rewrite settings with updated line}
		rewrite(f);
		for j := 1 to i do
			if j = index then {line, which shall be rewrited}
				begin
					if new <> '' then writeln(f, new); {'' means deleting line}
				end
			else writeln(f, data[j]);
		if index > i then writeln(f, new); {add new line}
		close(f);
	end;

{Graphic procedures}

procedure draw_block(x, y: integer; shape: matrix_4x4; color: byte);
	{Draws a blocks, x and y are coordinates of left top corner}
	var
		i, j: byte; {indices}
	
	begin
		textbackground(color);
		for i := 1 to 4 do {for rows}
			if y + i - 1 >= 3 then {if this row is within the visible space}
				for j := 1 to 4 do {for columns}
					if shape[i][j] then {if there is a pixel}
						begin
							gotoxy(2*x - 1 + (j-1)*2, y + i - 1);
							write('  ');
					end;
		normvideo;
	end;

procedure draw_frame;
	{Draws border of the whole screen}
	var
		i: byte; {index}

	begin
		{Draw top line}
		gotoxy(2, 1);
		for i := 1 to 78 do write('-');

		{Draw bottom line}
		gotoxy(2, 25);
		for i := 1 to 78 do write('-');

		{Draw left and right line}
		for i := 2 to 24 do
			begin gotoxy(1, i); write('|'); gotoxy(80, i); write('|'); end;
	end;

procedure draw_rect(x1, y1, x2, y2, color: byte);
	{Draws a rectangle, x1 and y1 are coordinates of top left corner and x2}
	{and y2 are coordinates of bottom right corner}
	begin
		window(2*x1-1, y1, 2*x2, y2);
		textbackground(color);
		clrscr;
		window(1, 1, 80, 25);
		normvideo;
	end;

procedure draw_button(x, y, w, h: byte; text: string; active: boolean);
	{Draws a button, x and y are coordinates of top left corner, w is width,}
	{h is height and active indicates if the button is focused or not}
	var
		len: byte; {length of text}
		left_padding, top_padding: byte; {paddings}
		color, text_color: byte; {colors;}

	begin
		{Set colors}
		if active then begin color := 4; text_color := 7; end
		else begin color := 7; text_color := 0; end;

		{Calculate parameters}
		len := length(text);
		left_padding := (2*w - len) div 2;
		top_padding := (h - 1) div 2;

		{Draw the button}
		draw_rect(x, y, x+w-1, y+h-1, color);
		gotoxy(2*x-1+left_padding, y+top_padding);
		textbackground(color);
		textcolor(text_color);
		write(text);
		normvideo;
	end;

procedure draw_footnote(text: string);
	{Draws a footnote}
	var
		i, j: byte; {indices}
		lines: byte; {number of lines}

	begin
		lines := (length(text) - 1) div 76 + 1;
		
		{Draw a separator}
		gotoxy(2, 24 - lines);
		for i := 2 to 79 do write('-');
		
		{Write all lines}
		for i := 1 to lines do
			begin
				gotoxy(3, 24 - lines + i);
				for j := 1 to min(76, length(text)-76*(i-1)) do
					write(text[(i-1)*76+j]);
			end;
	end;

procedure draw_headnote(text: string);
	{Draws a headnote}
	var
		i, j: byte; {indices}
		lines: byte; {number of lines}

	begin
		lines := (length(text) - 1) div 76 + 1;
		
		{Draw a separator}
		gotoxy(2, 2 + lines);
		for i := 2 to 79 do write('-');
		
		{Write all lines}
		for i := 1 to lines do
			begin
				gotoxy(3, 1 + i);
				for j := 1 to min(76, length(text)-76*(i-1)) do
					write(text[(i-1)*76+j]);
			end;
	end;

{Screen functions}

function menu: byte;
	{Everything for "menu" screen, returns next screen}
	var
		active: byte; {active button}
		ch: char; {for keyboard input}

	procedure draw(active: byte);
		{Draw whole "menu" screen}
		const
			buttons: array[1..3] of string = ('Play', 'Settings', 'Quit');

		var
			i: byte; {index}

		begin
			textbackground(bg_color);
			clrscr;
			draw_frame;
			draw_footnote('Space - select | Arrows up/down - move');
			for i := 1 to 3 do
				draw_button(16, i*5+1, 8, 3, buttons[i], active = i);
			gotoxy(80, 25);
			normvideo;
		end;

	begin
		active := 1;
		draw(active);

		while true do if keypressed then
			begin
				ch := readkey;

				if ch = #27 then {escape}
					begin menu := scr_quit; exit; end
				else if (ch = #72) and (active > 1) then {up}
					begin dec(active); draw(active); end
				else if (ch = #80) and (active < 3) then {down}
					begin inc(active); draw(active); end
				else if ch = #32 then {space}
					begin
						if active = 1 then menu := scr_game
						else if active = 2 then menu := scr_settings
						else if active = 3 then menu := scr_quit;
						exit;
					end;
			end;
	end;

function game: byte;
	{Everything for "game" screen, returns next screen}
	type
		matrix_20x32 = array[1..20, 1..32] of byte; {for the whole space}

	var
		ch: char; {for keyboard input}
		x_space: byte; {x coordinate of left edge of the space}
		active_block, next_block: byte; {id of the falling and next block}
		i, j: byte; {indices}
		new_lines: byte; {how many lines were erased recently}
		lines_to_speedup: byte; {how many lines before speed will increase}
		x, y, old_x, old_y: integer; {block coordinates before and after move}
		y1: real; {y coordinate of block, which will be rounded to integer}
		dy: real; {how much will y1 increase (corresponds to speed)}
		dcol_delay: real; {delta of col_delay}
		col_delay: real; {how much longer can blocks move after collision}
		col_delay_init: integer; {total time of "col_delay"}
		lines: integer; {total number of erased lines}
		space: matrix_20x32; {colors of whole space}
		shape, old_shape: matrix_4x4; {block shape before and after rotation}
		rotation: boolean; {indicates if a block were rotated recently}
		pause: boolean; {indicates if the game is paused}
		color, next_color: byte; {color of current and next block}
		next_shape: matrix_4x4; {shape of the next block}

	procedure draw_space(x_space: byte; space: matrix_20x32);
		{Draws only all pixels of the space}
		var
			i, j: byte; {indices}

		begin
			for i := 1 to 20 do {rows}
				begin
					gotoxy(x_space*2-1, i+2);
					for j := 1 to width do {columns}
						begin
							textbackground(space[i][j]);
							write('  ');
						end;
				end;
		end;

	procedure draw(x_space: byte; space: matrix_20x32);
		{Draws all pixels of the space and initial values of the right panel}
		begin
			textbackground(bg_color);
			clrscr;
			draw_frame;
			draw_space(x_space, space);

			{Draw right panel}
			draw_rect(36, 3, 39, 6, 7);
			
			gotoxy(71, 8); write('SCORE');
			gotoxy(71, 9); write('0');
			
			gotoxy(71, 11); write('BEST');

			gotoxy(71, 14); write('LINES');
			gotoxy(71, 15); write('0');

			gotoxy(71, 17); write('SPEED');
		end;

	function col_down(x, y: integer; space: matrix_20x32;
						shape: matrix_4x4): boolean;
		{Returns whether there is something below the active block}
		var
			i, j: byte; {indices}
			empty_row: boolean; {indicates if there is a pixel in the row}

		begin
			col_down := false;

			{Check if the block is at the bottom}
			for i := 1 to 4 do {rows}
				begin
					empty_row := true;
					for j := 1 to 4 do {columns}
						if shape[i][j] then empty_row := false;
					if not empty_row and (y + i - 1 = 20) then
						col_down := true;
				end;

			{Check if there is another block below the active block}
			for i := 1 to 4 do {rows}
				if (y + i >= 1) and (y + i <= 20) then
					for j := 1 to 4 do {columns}
						if (x + j - 1 >= 1) and (x + j - 1 <= width) then
							if (space[y+i][x+j-1] <> fg_color) and
								shape[i][j] then
								col_down := true;
		end;

	function col_up(y: integer; shape: matrix_4x4): boolean;
		{Returns if there is a pixel in the first (top) line}
		var
			i, j: byte; {indices}
			empty_row: boolean; {indicates if there is a pixel in the row}

		begin
			col_up := false;
			for i := 1 to 4	do
				begin
					empty_row := true;
					for j := 1 to 4 do
						if shape[i][j] then empty_row := false;
					if not empty_row and (y+i-1 <= 1) then col_up := true;
				end;
		end;

	function col_left(x, y: integer; space: matrix_20x32;
							shape: matrix_4x4): boolean;
		{Returns whether there is something left to the active block}
		var
			i, j: byte; {indices}
			empty_column: boolean; {indicates if there is a pixel in the row}

		begin
			col_left := false;

			{Check if the block at the left edge of the space}
			for i := 1 to 4 do {columns}
				begin
					empty_column := true;
					for j := 1 to 4 do {rows}
						if shape[j][i] then empty_column := false;
					if not empty_column and (x+i-1 <= 1) then
						col_left := true;
				end;

			{Check if there is another block left to the active block}
			for i := 1 to 4 do {columns}
				if (x + i - 2 >= 1) and (x + i - 2 <= width) then
					for j := 1 to 4 do {rows}
						if y + j - 1 > 0 then 
							if shape[j][i] and (space[y+j-1][x+i-2] <> fg_color)
								then col_left := true;


		end;

	function col_right(x, y: integer; space: matrix_20x32;
							shape: matrix_4x4): boolean;
		{Returns whether there is something right to the active block}
		var
			i, j: byte; {indices}
			empty_column: boolean; {indicates if there is a pixel in the row}

		begin
			col_right := false;

			{Check if the block at the right edge of the space}
			for i := 1 to 4 do {columns}
				begin
					empty_column := true;
					for j := 1 to 4 do {rows}
						if shape[j][i] then empty_column := false;
					if not empty_column and (x+i-1 >= width) then
						col_right := true;
				end;

			{Check if there is another block right to the active block}
			for i := 1 to 4 do {columns}
				if (x + i >= 1) and (x + i <= width) then
					for j := 1 to 4 do {rows}
						if y + j - 1 > 0 then
							if shape[j][i] and (space[y+j-1][x+i] <> fg_color)
								then col_right := true;
		end;

	function check_lines(var space: matrix_20x32): byte;
		{Erase all full lines and returns how many of them have been erased}
		var
			i, j, k: byte; {indices}
			total: byte; {how many lines are full}
			erase_line: boolean; {if the line should be erased}

		begin
			total := 0;

			for i := 20 downto 1 do {lines (from bottom to top)}
			begin
				erase_line := true;
				while erase_line do
					begin
						erase_line := true;
						for j := 1 to width do {columns}
							if space[i][j] = fg_color then erase_line := false;
						if erase_line then
							begin
								inc(total);
								for j := i downto 2 do {lines}
									for k := 1 to width do {columns}
										space[j][k] := space[j-1][k];
								for j := 1 to width do {first line}
									space[1][j] := fg_color;
							end;
					end;
			end;

			check_lines := total;
		end;

	procedure rotate(var x: integer; y: integer; space: matrix_20x32;
						var shape: matrix_4x4);
		{Rotates the active block clockwise}
		var
			i, j: byte; {indices}
			new: matrix_4x4; {shape after rotation}

		begin
			{Calculates "new shape"}
			for i := 1 to 4 do
				for j := 1 to 4 do
					new[j][4-i+1] := shape[i][j];

			{Check if block after rotation fits into the space}
			for i := 0 to 3 do {checks possible shift sizes}
				for j := 0 to 1 do {checks possible shift directions (L/R)}
					if not col_left(x+i*(2*j-1)+1, y, space, new) and
					not col_right(x+i*(2*j-1)-1, y, space, new) then
						begin
							shape := new;
							x := x+i*(2*j-1);
							exit;
						end;
		end;

	function new_block(blocks: list; nu_blocks: byte): byte;
		{Returns id of new block}
		var
			i: byte; {index}
			total_chances: integer; {sum of all block chances}
			chosen: integer; {randomly chosen block}

		begin
			{Calculates "total_chances"}
			total_chances := 0;
			for i := 1 to nu_blocks do
				if blocks[i].active then
					total_chances := total_chances + blocks[i].chance;

			{Returns chosen block}
			chosen := random(total_chances) + 1;
			for i := 1 to nu_blocks do
				if blocks[i].active then
					if chosen > blocks[i].chance then
						chosen := chosen - blocks[i].chance
					else
						begin
							new_block := i;
							exit;
						end;
		end;

	function read_best: longint;
		{Returns best score}
		var
			f: text; {file}
			score: longint; {output}

		begin
			assign(f, 'best.txt');

			{Create the file if it does not already exist}
			if not exists('best.txt') then
				begin
					rewrite(f);
					write(f, '0');
					close(f);
				end;

			{Read score}
			reset(f);
			read(f, score);
			read_best := score;
			close(f);
		end;

	procedure save_best(score: longint);
		{Saves new best score}
		var
			f: text; {file}

		begin
			assign(f, 'best.txt');
			rewrite(f);
			write(f, score);
			close(f);
		end;

	begin
		{initialize variables}
		read_settings(speed_correlation, speed, width, nu_blocks, blocks);
		best := read_best;
		x := width div 2 - 1;
		y1 := -3;
		y := round(y1);
		col_delay_init := 500;
		col_delay := col_delay_init;
		score := 0;
		lines := 0;
		lines_to_speedup := 0;
		pause := false;
		
		{Choose current and next block}
		randomize;
		active_block := new_block(blocks, nu_blocks);
		shape := blocks[active_block].shape;
		color := blocks[active_block].color;
		next_block := new_block(blocks, nu_blocks);
		next_shape := blocks[next_block].shape;
		next_color := blocks[next_block].color;

		{Create space}
		x_space := (40 - width) div 2 - 2;
		for i := 1 to 20 do
			for j := 1 to width do
				space[i][j] := fg_color;
		draw(x_space, space);
		gotoxy(71, 18); write(speed);
		gotoxy(71, 12); write(best);
		draw_block(36, 3, next_shape, next_color); {next}
		draw_block(x + x_space - 1, y+2, shape, color); {current}

		{Game loop}
		while true do
			begin
				if col_delay <= 0 then {if the block can be no longer moved}
					begin
						score := score + speed;
						
						{Game over}
						if col_up(y, shape) then
							begin
								if score > best then save_best(score);
								game := scr_loss;
								exit;
							end;

						{Redraw space}
						for i := 1 to 4 do
							for j := 1 to 4 do
								if shape[i][j] then
									space[y+i-1][x+j-1] := color;

						{Erase lines and update score}
						new_lines := check_lines(space);
						lines := lines + new_lines;
						if new_lines = 1 then score := score + 50*speed
						else if new_lines = 2 then score := score + 125*speed
						else if new_lines = 3 then score := score + 200*speed
						else if new_lines = 4 then score := score + 300*speed;
						draw_space(x_space, space);
						
						{Update right panel}
						textbackground(bg_color);
						gotoxy(71, 9); write(score);
						gotoxy(71, 15); write(lines);

						if score > best then
							begin
								gotoxy(71, 12);
								write(score);
							end;

						lines_to_speedup := lines_to_speedup + new_lines;
						if lines_to_speedup >= 10 then
							begin
								lines_to_speedup := lines_to_speedup mod 10;
								if speed < 9 then inc(speed);
								gotoxy(71, 18); write(speed);
							end;

						{Reset block}
						x := width div 2 - 1;
						y1 := -3;
						y := round(y1);

						active_block := next_block;
						randomize;
						next_block := new_block(blocks, nu_blocks);
						shape := next_shape;
						color := next_color;	
						next_shape := blocks[next_block].shape;
						next_color := blocks[next_block].color;
						
						draw_rect(36, 3, 39, 6, 7);
						draw_block(36, 3, next_shape, next_color); {next}
						draw_block(x + x_space - 1, y+2, shape, color);

						col_delay := col_delay_init;
					end
				else
					begin
						if not pause then
							begin
								{Determine dy}
								if col_down(x, y, space, shape) then
									begin
										dcol_delay := - speed_correlation / 20;
										col_delay := col_delay + dcol_delay;
										dy := 0;
									end
								else
									begin
										col_delay := col_delay_init;
										dy := speed_correlation * speed;
										dy := dy / 10000;
									end;

								{Initialize variables}
								old_x := x;
								old_y := y;
								old_shape := shape;
								rotation := false;

								{Clear "pause" text}
								gotoxy(71, 20); write('     ');
								gotoxy(80, 25);
							end
						else
							begin
								textcolor(red);
								gotoxy(71, 20); write('PAUSE');
								gotoxy(80, 25);
								normvideo;
							end;

						{Keyboard input}
						if keypressed then
							begin
								ch := readkey;
								if ch = #27 then {escape}
									begin
										game := scr_menu;
										exit;
									end
								else if (ch = #80) and {down}
									not col_down(x, y, space, shape) and
									not pause then
										dy := 1
								else if (ch = #72) and {up}
									not col_down(x, y, space, shape) and
									not pause then
									begin
										rotate(x, y, space, shape);
										rotation := true;
									end
								else if (ch = #75) and {left}
									not col_left(x, y, space, shape) and
									not pause then
									dec(x)
								else if (ch = #77) and {right}
									not col_right(x, y, space, shape)
									and not pause then
									inc(x)
								else if (ch = #112) then
									pause := not pause;
							end;

						if not pause then
							begin	
								{Update varibles}
								y1 := y1 + dy;
								y := round(y1);

								{Redraw block}
								if (old_x <> x) or (old_y <> y)
									or rotation then
									begin
										draw_block(old_x + x_space - 1,
											old_y+2, old_shape, 7);
										draw_block(x + x_space - 1, y+2,
											shape, color);
									end;
							end;
					end;

				delay(1); {without this the game would run way too fast}
				gotoxy(80, 25);
			end;
	end;

function settings: byte;
	{Everything for "settings" screen, returns next screen}
	var
		active: byte; {active button}
		ch: char; {pressed key}

	procedure draw(active: byte);
		{Draws whole "settings" screen}
		var
			i: byte; {index}
			buttons: array[1..4] of string; {all buttons}
			temp: string; {temporary string used for different purposes}

		begin
			read_settings(speed_correlation, speed, width, nu_blocks, blocks);
			
			{Create first button}
			buttons[1] := 'Speed up/slow down';

			{Create second button}
			temp := byte_to_str(speed);
			buttons[2] := 'Speed: ' + temp + '/9';

			{Create third button}
			temp := byte_to_str(width);
			buttons[3] := 'Width: ' + temp + '/32';
			
			{Create fourth button}
			buttons[4] := 'Blocks';

			{Draw screen itself and all buttons}
			textbackground(bg_color);
			clrscr;
			draw_frame;
			temp := 'Arrows up/down - move | Arrows right/left - change ';
			temp := temp + '| Escape - back';
			draw_footnote(temp);
			for i := 1 to 4 do
				draw_button(15, i*5 - 2, 10, 3, buttons[i], active = i);
			gotoxy(80, 25);
			normvideo;
		end;

	begin
		active := 1;
		draw(active);

		{Setting loop enables keyboard input}
		while true do if keypressed then
			begin
				ch := readkey;
				if ch = #27 then {escape}
					begin
						settings := scr_menu;
						exit;
					end
				else if (ch = #72) and (active > 1) then {up}
					begin
						dec(active);
						draw(active);
					end
				else if (ch = #80) and (active < 4) then {down}
					begin
						inc(active);
						draw(active);
					end
				else if (ch = #75) or (ch = #77) then {left or right}
					begin
						if active = 1 then {change speed correlation constant}
							begin
								settings := scr_settings_correlation;
								exit;
							end
						else if active = 2 then {change speed}
							begin
								if (ch = #75) and (speed > 1) then
									dec(speed)
								else if (ch = #77) and (speed < 9) then
									inc(speed);
								save_settings(2, byte_to_str(speed));
							end
						else if active = 3 then {change width}
							begin
								if (ch = #75) and (width > 8) then
									dec(width)
								else if (ch = #77) and (width < 32) then
									inc(width);
								save_settings(3, byte_to_str(width));
							end
						else if active = 4 then {change blocks}
							begin
								settings := scr_settings_blocks;
								exit;
							end;
						draw(active);
					end
				else if ch = #32 then {space}
					begin
						if active = 4 then
							begin
								settings := scr_settings_blocks;
								exit;
							end
						else if active = 1 then
							begin
								settings := scr_settings_correlation;
								exit;
							end;
					end;
			end;
	end;

function settings_blocks: byte;
	{Everything for "settings blocks" screen, returns next screen}
	var
		active: byte; {active button}
		ch: char; {read key}

	procedure draw_tile(active, i, x, y: byte);
		{Draws a single tile}
		begin
			{Check if the tile is active}
			if active = i then draw_rect(x - 1, y - 1, x + 12, y + 4, 4);

			{Draw shape}
			draw_rect(x, y, x + 3, y + 3, 7);
			draw_block(x, y, blocks[i].shape, blocks[i].color);
			
			{Set text background}
			if active = i then textbackground(red)
			else textbackground(bg_color);

			{Write info}
			gotoxy(2*x + 9, y + 1);
			if blocks[i].active then write('Active: YES')
			else write('Active: NO');
			gotoxy(2*x + 9, y + 2);
			write('Usuality: ' + byte_to_str(blocks[i].chance) + '/9');
		end;

	procedure draw(active: byte);
		{Draws entire "settings_blocks" screen}
		var
			nu_pages: byte; {total number of pages (max 10)}
			page: byte; {current page}
			i: byte; {index}
			x, y: byte; {coordinates}
			temp: string; {temporary string used for different purposes}

		begin
			{Draw screen itself and (extra) frame}
			textbackground(bg_color);
			clrscr;
			draw_frame;
			for i := 4 to 22 do
				begin
					gotoxy(41, i);
					write('|');
				end;

			{Draw footnote and headnote}
			nu_pages := (nu_blocks - 1) div 6 + 1;
			page := (active - 1) div 6 + 1;

			temp := 'Page: ' + byte_to_str(page) + '/';
			temp := temp + byte_to_str(nu_pages);
			draw_headnote(temp);
			
			temp := 'Space - change | Arrows up/down - move | D - delete ';
			temp := temp + 'block | N - create a newblock | Escape - ';
			temp := temp + 'back';
			draw_footnote(temp);
			
			{Draw left tiles}
			for i := (page-1)*6 + 1 to min(nu_blocks, (page-1)*6 + 3) do
				begin
					x := 5;
					y := 5 + 6*((i-1) mod 6);
					draw_tile(active, i, x, y);
				end;

			{Draw right tiles}
			for i := (page-1)*6 + 4 to min(nu_blocks, (page-1)*6 + 6) do
				begin
					x := 25;
					y := 5 + 6*((i-1) mod 6 - 3);
					draw_tile(active, i, x, y);
				end;

			gotoxy(80, 25);
			normvideo;
		end;

	begin
		read_settings(speed_correlation, speed, width, nu_blocks, blocks);
		active := 1;
		draw(active);

		{Loop for keyboard input}
		while true do
			if keypressed then
				begin
					ch := readkey;
					if ch = #27 then {escape}
						begin
							settings_blocks := scr_settings;
							exit;
						end
					else if (ch = #72) and (active > 1) then {up}
						begin
							dec(active);
							draw(active);
						end
					else if (ch = #80) and (active < nu_blocks) then {down}
						begin
							inc(active);
							draw(active);
						end
					else if ch = #32 then {space}
						begin
							settings_blocks := scr_block_editor;
							edited_block := active;
							exit;
						end
					else if (ch = #100) or (ch = #68) then {D/d}
						begin
							save_settings(active+3, '');
							read_settings(speed_correlation, speed, width,
										  nu_blocks, blocks);
							active := 1;
							draw(active);
						end
					else if ((ch = #110) or (ch = #78)) and (nu_blocks<60) then
						begin
							{Save new (empty) block}
							save_settings(nu_blocks+4+1, '0000000000000000151');
							settings_blocks := scr_block_editor;
							edited_block := nu_blocks+1;
							exit;
						end;
				end;
	end;

function block_editor: byte;
	{Everything for "block editor" screen, returns next screen}
	var
		i, j: byte; {indices}
		active: byte; {active button (/pixel while editing shape)}
		empty: boolean; {is new block empty (saving them is not possible)}
		ch: char; {read key}
		temp: string; {temporary string for long text}

	procedure draw(active: byte);
		{Draws everything on this screen}
		var
			i: byte; {index}
			x, y: byte; {coordinates}
			buttons: array[1..3] of string; {all button labels}

		begin
			read_settings(speed_correlation, speed, width, nu_blocks, blocks);
			
			{First button}
			if blocks[edited_block].active = true then
				buttons[1] := 'Active: YES'
			else buttons[1] := 'Active: NO';

			{Second button}
			buttons[2] := 'Usuality: ';
			buttons[2] := buttons[2] + byte_to_str(blocks[edited_block].chance);
			buttons[2] := buttons[2] + '/9';

			{Third button}
			buttons[3] := 'Color: ';
			buttons[3] := buttons[3] + byte_to_str(blocks[edited_block].color);
			buttons[3] := buttons[3] + '/6';

			{Draw screen itself}
			textbackground(bg_color);
			clrscr;
			draw_frame;
			temp := 'Arrows up/down - move | Arrow right/left - move ';
			temp := temp + 'when changing shape / changeelse | Space - ';
			temp := temp + 'insert/remove block at [] | Escape - back';
			draw_footnote(temp);

			{Draw buttons}
			for i := 1 to 3 do
				draw_button(15, i*5 - 2, 10, 3, buttons[i], active = i);

			{Draw block}
			x := 18;
			y := 18 + 6*((1-1) mod 6);
			draw_rect(x, y, x + 3, y + 3, fg_color);
			draw_block(x, y, blocks[edited_block].shape,
				blocks[edited_block].color);

			{Draw cursor}
			if active > 3 then
				begin
					x := 2*x + 2*(active mod 4) - 1;
					y := y + (active - 4) div 4;
					gotoxy(x, y);

					textcolor(0);
					x := active mod 4 + 1;
					y := active div 4;
					if blocks[edited_block].shape[y][x] then
						textbackground(blocks[edited_block].color)
					else
						textbackground(fg_color);

					write('[]');
				end;

			gotoxy(80, 25);
			normvideo;
		end;

	procedure save_helper(active: byte);
		{Creates string from "block_editor", which can be written to settings}
		var
			i, j: byte; {indices}
			new: string; {new line, which will be inserted to the settings}

		begin
			new := '';

			{Prepare info about the shape}
			for i := 1 to 4 do {rows}
				for j := 1 to 4 do {columns}
					if active-3 = (i-1)*4 + j then {change value under cursor}
						if blocks[edited_block].shape[i][j] then
							new := new + '0'
						else new := new + '1'
					else {leave without change, only convert}
						if blocks[edited_block].shape[i][j] then
							new := new + '1'
						else new := new + '0';
			
			{Prepare info about activeness, chance and color}
			if blocks[edited_block].active then new := new + '1'
			else new := new + '0';
			new := new + byte_to_str(blocks[edited_block].chance);
			new := new + byte_to_str(blocks[edited_block].color);

			save_settings(edited_block+3, new);
		end;

	begin
		active := 1;
		draw(active);

		{Loop for keyboard input}
		while true do if keypressed then
			begin
				ch := readkey;
				if ch = #27 then {escape}
					begin
						{Is it an empty block (which will not be saved)}
						empty := true;
						for i := 1 to 4 do
							for j := 1 to 4 do
								if blocks[edited_block].shape[i][j] then
									empty := false;
						if empty then save_settings(edited_block+3, '');
						
						block_editor := scr_settings_blocks;
						exit;
					end
				else if ch = #72 then {up}
					begin
						if (active > 1) and (active < 4) then dec(active)
						else if (active > 1) and (active < 8) then active := 3
						else if active > 1 then active := active - 4;
						draw(active);
					end
				else if ch = #80 then {down}
					begin
						if active < 4 then inc(active)
						else if active < 16 then active := active + 4;
						draw(active)
					end
				else if ch = #75 then {left}
					begin
						if active = 1 then
							begin
								blocks[edited_block].active :=
									not blocks[edited_block].active;
								save_helper(active);
							end
						else if (active = 2) and
							(blocks[edited_block].chance > 1) then
							begin
								dec(blocks[edited_block].chance);
								save_helper(active);
							end
						else if (active = 3) and
							(blocks[edited_block].color > 1) then
							begin
								dec(blocks[edited_block].color);
								save_helper(active);
							end
						else if (active > 3) and (active mod 4 <> 0) then
							dec(active);

						draw(active);
					end
				else if (ch = #77) then {right}
					begin
						if (active = 1) then
							begin
								blocks[edited_block].active :=
									not blocks[edited_block].active;
								save_helper(active);
							end
						else if (active = 2) and
							(blocks[edited_block].chance < 9) then
							begin
								inc(blocks[edited_block].chance);
								save_helper(active);
							end
						else if (active = 3) and
							(blocks[edited_block].color < 6) then
							begin
								inc(blocks[edited_block].color);
								save_helper(active);
							end
						else if (active > 3) and (active mod 4 <> 3) then
							inc(active);
						
						draw(active);
					end
				else if (ch = #32) and (active > 3) then {space}
					begin
						save_helper(active);
						draw(active);
					end;
			end;
	end;

function loss: byte;
	{Everything for "loss" screen, returns next screen}
	var
		active: byte; {active button}
		ch: char; {for keyboard input}

	procedure draw(active: byte);
		{Draws whole loss screen}
		const
			buttons: array[1..2] of string = ('Play again', 'Menu');

		var
			i: byte; {index}

		begin
			textbackground(bg_color);
			clrscr;
			draw_frame;

			{Write messages with (best) score}
			gotoxy(31, 5); write('Game over!');
			gotoxy(31, 7); write('Your score: '); write(score);	
			gotoxy(31, 8);
			if score > best then write('New best score!')
			else begin write('Best score: '); write(best); end;

			{Draw rest of screen}
			draw_footnote('Space - select | Arrows up/down - move');
			for i := 1 to 2 do
				draw_button(16, i*5 + 6, 8, 3, buttons[i], active = i);
			
			gotoxy(80, 25);
			normvideo;
		end;

	begin
		active := 1;
		draw(active);

		{Loop for selection}
		while true do if keypressed then
			begin
				ch := readkey;
				if (ch = #72) and (active > 1) then {up}
					begin
						dec(active);
						draw(active);
					end
				else if (ch = #80) and (active < 2) then {down}
					begin
						inc(active);
						draw(active);
					end
				else if ch = #32 then {space}
					begin
						if active = 1 then loss := scr_game
						else if active = 2 then loss := scr_menu;
						exit;
					end;
			end;
	end;

function settings_correlation: byte;
	{Everything for "settings_correlation" screen, returns next screen}
	var
		active: byte; {active button}
		ch: char; {for keyboard input}
		count: longint;
		even: boolean;

	procedure draw(active: byte);
		{Draws whole settings_correlation screen}
		var
			msg: string;

		begin
			textbackground(bg_color);
			clrscr;
			draw_frame;
			msg := 'Set speed constant. The square should be changing color on';
			msg := msg + ' your computer    color approximately once per secon';
			msg := msg + 'd. The constant is natural number from 1  to 1000. T';
			msg := msg + 'he more, the faster. Arrows left/right decrease/incr';
			msg := msg + 'ease by 1 and arrows down/up by 20';
			draw_footnote(msg);

			{Write current value of the constant}
			gotoxy(29, 5); write('Current constant: ', speed_correlation);
		end;

	procedure update_shown_value;
		var
			msg: string;

		begin
			msg := 'Current constant: ';
			msg := msg + int_to_str(speed_correlation);
			msg := msg + '   ';
			gotoxy(29, 5); write(msg);
		end;

	begin
		active := 1;
		draw(active);
		count := 0;
		draw_rect(19, 10, 22, 13, blue);
		even := true;

		{Loop for selection}
		while true do
			begin
				count := count + speed_correlation;
				if count >= 20000 then
					begin
						count := 0;
						if even then draw_rect(19, 10, 22, 13, red)
						else draw_rect(19, 10, 22, 13, blue);
						even := not even;
					end;

				if keypressed then
					begin
						ch := readkey;
						if (ch = #72) and (speed_correlation < 981) then
							{up}
							begin
								speed_correlation := speed_correlation + 20;
								save_settings(1, int_to_str(speed_correlation));
								update_shown_value;
							end
						else if (ch = #80) and (speed_correlation > 20) then
							{down}
							begin
								speed_correlation := speed_correlation - 20;
								save_settings(1, int_to_str(speed_correlation));
								update_shown_value;
							end
						else if (ch = #77) and (speed_correlation < 1000) then
							{right}
							begin
								inc(speed_correlation);
								save_settings(1, int_to_str(speed_correlation));
								update_shown_value;
							end
						else if (ch = #75) and (speed_correlation > 1) then
							{left}
							begin
								dec(speed_correlation);
								save_settings(1, int_to_str(speed_correlation));
								update_shown_value;
							end
						else if ch = #27 then {escape}
							begin
								settings_correlation := scr_settings;
								exit;
							end;
					end;

				delay(1);
			end;
	end;
begin
	{Draw main screen}
	window(1, 1, 80, 25);
	textbackground(bg_color);
	clrscr;

	{Main loop}
	scr := scr_menu;
	while scr <> scr_quit do
		if scr = scr_menu then scr := menu
		else if scr = scr_game then scr := game
		else if scr = scr_settings then scr := settings
		else if scr = scr_settings_blocks then scr := settings_blocks
		else if scr = scr_block_editor then scr := block_editor
		else if scr = scr_loss then scr := loss
		else if scr = scr_settings_correlation then scr := settings_correlation;
	
	{Clear the scr}
	normvideo;
	clrscr;
end.